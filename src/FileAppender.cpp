//----------------------------------------------------------------------------
// Copyright (c) 2004-2016 The Tango Community
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the GNU Lesser Public License v3
// which accompanies this distribution, and is available at
// http://www.gnu.org/licenses/lgpl.html
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// YAT4Tango LIBRARY
//----------------------------------------------------------------------------
//
// Copyright (C) 2006-2016 The Tango Community
//
// The YAT4Tango library is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.
//
// The YAT4Tango library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// See COPYING file for license details
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//------------------------------------------------------------------------------
/*!
 * \authors See AUTHORS file
 */

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iomanip>
#include <time.h>
#include <yat/threading/Mutex.h>
#include <yat/threading/Task.h>
#include <yat/file/FileName.h>
#include <yat4tango/FileAppender.h>
#include <yat4tango/InnerAppender.h>

namespace yat4tango
{

const std::string PREVIOUS_SESSION_FOLDER = "previous_session";

//=============================================================================
// class FileAppenderWatcher
//=============================================================================
class FileAppenderWatcher : public yat::Task
{
public:
  FileAppenderWatcher(const std::string& path, const std::string& name,
                      std::size_t max_log_days, yat::Mutex* file_mtx_p)
  {
    std::ostringstream oss;
    oss << name << ".log.*";
    m_pattern = oss.str();
    m_path = path;
    m_name = name;
    m_max_log_days = max_log_days;
    m_mtx_p = file_mtx_p;
  }

  void new_file()
  {
    m_last_file_creation_date = yat::CurrentTime();
  }

  void check_file()
  {
    yat::FileName f(m_path, m_name, std::string("log"));

    if( f.file_exist() )
    {
      double age = yat::Duration(yat::CurrentTime(), m_last_file_creation_date).total_secs();
      yat::uint64 file_size = f.size64();

      if( (file_size > YAT_MBYTES && age > SEC_PER_MIN) || age > SEC_PER_DAY )
      {
        rename_file();
      }
    }
  }

  void rename_file()
  {
    yat::Time tm;
    yat::DateFields df;
    yat::FileName src(m_path, m_name, "log");

    src.mod_time(&tm, true);
    tm.get(&df);

    yat::FileName dest(m_path,
                     yat::StringUtil::str_format("%s.log.%04d%02d%02d%02d%02d",
                                                 m_name.c_str(),
                                                 df.year, df.month, df.day,
                                                 df.hour, df.min));
    { yat::AutoMutex<yat::Mutex> _lock(*m_mtx_p);
      if( !dest.file_exist() )
        src.rename(dest.full_name());
    }
  }

  void cleanup()
  {
    YAT_TRACE("FileAppenderWatcher::cleanup");
    yat::FileEnum fe(m_path);
    std::vector<std::string> to_delete;
    while( fe.find() )
    {
      if( yat::StringUtil::match(fe.name_ext(), m_pattern) )
      {
        yat::CurrentTime tm_cur;
        yat::Time tm_file;
        fe.mod_time(&tm_file, true);
        double file_age = yat::Duration(tm_file, yat::CurrentTime()).total_secs();
        if( file_age > m_max_log_days * 86400 )
          to_delete.push_back(fe.full_name());
      }
    }
    yat::AutoMutex<yat::Mutex> _lock(*m_mtx_p);
    for( std::size_t i = 0; i < to_delete.size(); ++i )
    {
      yat::FileName fn(to_delete[i]);
      fn.remove();
    }
  }

  void arch()
  {
    yat::AutoMutex<yat::Mutex> _lock(*m_mtx_p);
    YAT_TRACE("FileAppenderWatcher::arch");
    yat::FileName fn_dst(m_path);
    fn_dst.join(PREVIOUS_SESSION_FOLDER);
    if( !fn_dst.path_exist() )
      fn_dst.mkdir();
    else
      // delete directory content
      fn_dst.rmdir(false, true);

    yat::FileEnum fe(m_path);
    while( fe.find() )
    {
      fe.move(fn_dst.path());
    }
  }

protected:
  void handle_message (yat::Message& msg)
  {
    switch( msg.type() )
    {
      case yat::TASK_INIT:
        set_periodic_msg_period(1000 * SEC_PER_MIN);
        enable_periodic_msg(true);
        break;
      case yat::TASK_PERIODIC:
        cleanup();
      default:
        break;
    }
  }

private:

  std::string m_path;
  std::string m_name;
  std::string m_pattern;
  yat::Mutex* m_mtx_p;
  std::size_t m_max_log_days;
  yat::Time   m_last_file_creation_date;
};

// ============================================================================
// FileAppender static members
// ============================================================================
yat::Mutex FileAppender::m_rep_lock;
FileAppender::FileAppenderRepository FileAppender::m_rep;
std::string FileAppender::m_name;
std::string FileAppender::m_path;
std::string FileAppender::m_full_name;
yat::UniquePtr<class FileAppenderWatcher, yat::TaskExiter> FileAppender::m_watcher_ptr;

// ============================================================================
// FileAppender::initialize
// ============================================================================
void FileAppender::initialize(Tango::DeviceImpl* hd, std::size_t max_log_days,
                              const std::string& path)
{
  initialize(hd, true, max_log_days, path);
}

// ============================================================================
// FileAppender::initialize
// ============================================================================
void FileAppender::initialize(Tango::DeviceImpl* hd, bool load_previous_logs,
                              std::size_t max_log_days, const std::string& path)
{
  yat::MutexLock guard(FileAppender::m_rep_lock);

  if( !hd )
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "unexpected null pointer to Tango device!",
                    "yat4tango::FileAppender::initialize");
  }

  Tango::Util* util = Tango::Util::instance();
  std::string ds_name, instance, ds_full_name = util->get_ds_name();
  yat::StringUtil::split(ds_full_name, '/', &ds_name, &instance);

  // set full name : base_path/ds_name/instance/instance.log
  yat::FileName fn(path, instance, "log");
  fn.join(ds_name);
  fn.join(instance);

  if( !FileAppender::m_watcher_ptr )
  {
    FileAppender::m_full_name = fn.full_name();
    FileAppender::m_path = fn.path();
    FileAppender::m_name = fn.name();
    FileAppender::m_watcher_ptr.reset(new FileAppenderWatcher(FileAppender::m_path,
                                                              FileAppender::m_name,
                                                              max_log_days,
                                                              &FileAppender::m_rep_lock));
  }

  //- do we already have an FileAppender registered for the specified device?
  FileAppenderIterator it = FileAppender::m_rep.find(hd);
  if( it != FileAppender::m_rep.end() )
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "an FileAppender is already associated to the specified device",
                    "FileAppender::initialize");
  }

  FileAppender* fa = new (std::nothrow) FileAppender;
  if( !fa )
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "yat4tango::FileAppender instanciation failed!",
                    "yat4tango::FileAppender::initialize");
  }

  fa->initialize_i(hd);

  //- insert the FileAppender into the local repository
  std::pair<FileAppenderIterator, bool> insertion_result;
  insertion_result = FileAppender::m_rep.insert(FileAppenderEntry(hd, fa));
  if (insertion_result.second == false)
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "failed to insert the FileAppender into the local repository",
                    "yat4tango::FileAppender::initialize");
  }

  if( load_previous_logs )
    fill_inner_appender(hd);

  if( FileAppender::m_watcher_ptr )
  {
    if( fn.file_exist() )
      FileAppender::m_watcher_ptr->rename_file();
    // archive log files from previous session
    FileAppender::m_watcher_ptr->arch();
    // start task
    FileAppender::m_watcher_ptr->go();
  }
}

// ============================================================================
// FileAppender::reset_i
// ============================================================================
void FileAppender::reset_i()
{
  FileAppender::m_watcher_ptr->arch();
}

// ============================================================================
// FileAppender::reset
// ============================================================================
void FileAppender::reset(Tango::DeviceImpl * hd)
{
  yat::MutexLock guard(FileAppender::m_rep_lock);

  if (! hd)
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "unexpected null pointer to Tango device!",
                    "yat4tango::FileAppender::release");
  }

  //- do we have an FileAppender registered for the specified device?
  FileAppenderIterator it = FileAppender::m_rep.find(hd);
  if (it == FileAppender::m_rep.end())
    THROW_DEVFAILED("DEVICE_ERROR",
                    "No FileAppender registered for this device!",
                    "yat4tango::FileAppender::reset");

  it->second->reset_i();
}

// ============================================================================
// FileAppender::release
// ============================================================================
void FileAppender::release(Tango::DeviceImpl * hd)
{
  yat::MutexLock guard(FileAppender::m_rep_lock);

  if (! hd)
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "unexpected null pointer to Tango device!",
                    "yat4tango::FileAppender::release");
  }

  //- do we have an FileAppender registered for the specified device?
  FileAppenderIterator it = FileAppender::m_rep.find(hd);
  if (it == FileAppender::m_rep.end())
    return;

  //- release the FileAppender
  //it->second->release_i();

  //- remove the FileAppender from the local repository
  FileAppender::m_rep.erase(it);

  FileAppender::m_watcher_ptr.reset();
}

// ============================================================================
// FileAppender::fill_inner_appender
// ============================================================================
void FileAppender::fill_inner_appender(Tango::DeviceImpl* hd)
{
  //- do we have an FileAppender registered for the specified device?
  FileAppenderIterator it = FileAppender::m_rep.find(hd);
  if (it == FileAppender::m_rep.end())
    THROW_DEVFAILED("DEVICE_ERROR",
                    "No FileAppender registered for this device!",
                    "yat4tango::FileAppender::reset");

  it->second->fill_inner_appender_i();
}

// ============================================================================
// FileAppender::fill_inner_appender_i
// ============================================================================
void FileAppender::fill_inner_appender_i()
{
  try
  {
    std::string dev_key = "| " + m_dev_name + " |";
    std::deque<std::string> messages;
    yat::FileName fn_dst(m_path);
    std::vector<yat::String> names;

    if( fn_dst.path_exist() )
    {
      yat::FileEnum fe(m_path);
      while( fe.find() )
      {
        names.push_back(fe.full_name());
      }
      std::sort(names.begin(), names.end());
      if( names.size() > 1 )
      {
        if( names[0].match("*.log") )
        {
          // This file contains the most recent messages, we move its
          // name at the end of the list
          names.push_back(names[0]);
          names.erase(names.begin());
        }
      }

      std::size_t max_depth = InnerAppender::get_max_buffer_depth(m_dev);

      for( std::size_t i = 0; i < names.size(); ++i )
      {
        // Load log file content
        yat::File fi(names[names.size() - i - 1]);
        std::string content;
        fi.load(&content);

        std::string msg;
        if( !content.empty() )
        {
          // Browse content in reverse order and fill container
          std::size_t current_pos = content.size()-1, next_pos = 0;
          while( next_pos != std::string::npos && messages.size() < max_depth )
          {
            next_pos = content.rfind('\n', current_pos);
            if( next_pos != std::string::npos )
              msg = content.substr(next_pos+1, current_pos - next_pos);
            else
              msg = content.substr(0, current_pos);

            if( msg.find(dev_key) != std::string::npos )
              messages.insert(messages.begin(), msg);
            current_pos = next_pos - 1;
          }
        }
        if( messages.size() >= max_depth )
          // buffer full
          break;
      }
    }

    if( !messages.empty() )
    {
      InnerAppender::fill(m_dev, messages);
    }
  }
  catch(...)
  {
    // Silent catch
  }
}

// ============================================================================
// FileAppender::FileAppender
// ============================================================================
FileAppender::FileAppender()
  : log4tango::Appender("file-appender"), m_dev(0), m_need_check_file_exist(true)
{
  //- noop
}

// ============================================================================
// FileAppender::~FileAppender
// ============================================================================
FileAppender::~FileAppender()
{
}

// ============================================================================
// FileAppender::initialize_i
// ============================================================================
void FileAppender::initialize_i(Tango::DeviceImpl* hd)
{
  //- check input
  if( !hd )
  {
    THROW_DEVFAILED("DEVICE_ERROR",
                    "unexpected null pointer to Tango device!",
                    "yat4tango::FileAppender::init");
  }
  //- store host device
  m_dev = hd;

  //- self register to device logger
  m_dev->get_logger()->add_appender(this);

  m_dev_name = hd->get_name();
}

// ============================================================================
// FileAppender::close
// ============================================================================
void FileAppender::close()
{
  //- noop
}

// ============================================================================
// FileAppender::
// ============================================================================
int FileAppender::_append(const log4tango::LoggingEvent& event)
{
  //------------------------------------------------------------
  //- DO NOT LOG FROM THIS METHOD !!!
  //------------------------------------------------------------
  static const size_t max_time_str_len = 32;

  try
  {
    //- reformat the incomming LoggingEvent
    //- date | LEVEL | thr_id | logger | message
    std::ostringstream oss;
    oss << yat::CurrentTime().to_ISO8601_micro()
        << " | ";
    oss.width(5);
    oss << std::left << log4tango::Level::get_name(event.level)
        << " | ";
    oss << std::hex << std::uppercase << event.thread_id << " | ";
    oss << m_dev_name << " | "
        << event.message
        << '\n';

    //- push the message into the log file
    yat::File f(m_full_name);

    { yat::MutexLock guard(FileAppender::m_rep_lock);

      try
      {
        if( m_need_check_file_exist )
        {
          if( !f.file_exist() )
          {
            if( !f.path_exist() )
              f.mkdir();

            FileAppender::m_watcher_ptr->new_file();
          }
          m_need_check_file_exist = false;
        }
        f.append(oss.str());
      }
      catch(...)
      {
        return -1;
      }

      // minimal interval between two calls to check_file is limited
      YAT_FREQUENCY_LIMITED_STATEMENT(             \
        FileAppender::m_watcher_ptr->check_file(); \
        m_need_check_file_exist = true,            \
        2); // seconds
    }
  }
  catch (...)
  {
    return -1;
  }
  return 0;
}

// ============================================================================
// FileAppender::requires_layout
// ============================================================================
bool FileAppender::requires_layout () const
{
  return false;
}

// ============================================================================
// FileAppender::set_layout
// ============================================================================
void FileAppender::set_layout (log4tango::Layout* l)
{
  delete l;
}

} // namespace

